//
//  main.m
//  ToyProject
//
//  Created by muyexi on 6/27/14.
//  Copyright (c) 2014 muyexi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WDAppDelegate class]));
        // dd
    }
}
