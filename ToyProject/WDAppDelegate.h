//
//  WDAppDelegate.h
//  ToyProject
//
//  Created by muyexi on 6/27/14.
//  Copyright (c) 2014 muyexi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
